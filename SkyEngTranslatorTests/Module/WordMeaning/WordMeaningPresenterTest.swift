//
//  WordMeaningPresenterTest.swift
//  SkyEngTranslatorTests
//
//  Created by Александр Лыков on 10.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import XCTest

final class WordMeaningPresenterTest: XCTestCase {

	private var presenter: WordMeaningPresenter?
	private var inputModel: WordMeaningInputModel?

    override func setUp() {
		presenter = WordMeaningPresenter(apiService: ApiServiceManagerMock())

		let testString = "testString"
		let meaning = SearchMeaningModel(
			id: 0,
			partOfSpeech: .noun,
			translation: TranslationModel(text: testString, note: nil),
			transcription: nil,
			soundURL: nil,
			imageURL: URL(string: "test.com"),
			previewURL: nil
		)

		inputModel = WordMeaningInputModel(word: testString, meaning: meaning)
    }

    override func tearDown() {
        presenter = nil
		inputModel = nil
    }

	/// Проверяем, что установленные данные корректно возвращаются (для отображения)
    func testPresenterSetup() {
		guard let presenter = presenter, let inputModel = inputModel else { return XCTFail() }

		presenter.setup(with: inputModel)

		let title = inputModel.word
		XCTAssertEqual(presenter.title, title)
		XCTAssertEqual(presenter.titleHeader != nil, title != nil)

		let subtitle = inputModel.meaning?.translation.text
		XCTAssertEqual(presenter.subtitle, subtitle)
		XCTAssertEqual(presenter.subtitleHeader != nil, subtitle != nil)

		if case let .url(url, _) = presenter.imageFormItem, let imageURL = inputModel.meaning?.imageURL {
			XCTAssertEqual(url, imageURL)
		} else {
			XCTFail()
		}
    }
}
