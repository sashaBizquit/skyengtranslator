//
//  UILabel+HidingText.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 10.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import UIKit

extension UILabel {
	var hidingText: String? {
		get {
			text
		}
		set {
			text = newValue
			isHidden = newValue?.isEmpty ?? true
		}
	}
}
