//
//  KeyedDecodingContainer.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 08.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import Foundation

protocol Transformer {
	associatedtype Input
	associatedtype Output
	func transform(_ decoded: Input) throws -> Output
}

extension KeyedDecodingContainer {
	func decode<T: Decodable>(_ key: Self.Key) throws -> T {
		return try decode(T.self, forKey: key)
	}

	func decodeIfPresent<T: Decodable>(_ key: Self.Key) throws -> T? {
		return try decodeIfPresent(T.self, forKey: key)
	}

	func decode<T: Transformer>(_ key: Self.Key, transformer: T) throws -> T.Output where T.Input: Decodable {
		let decoded: T.Input = try decode(key)
		return try transformer.transform(decoded)
	}

	func decodeIfPresent<T: Transformer>(_ key: Self.Key, transformer: T) throws -> T.Output? where T.Input: Decodable {
		guard let decoded: T.Input = try decodeIfPresent(key) else { return nil }
		return try transformer.transform(decoded)
	}
}
