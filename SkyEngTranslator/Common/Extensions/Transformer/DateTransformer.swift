//
//  DateTransformer.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 10.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import Foundation

enum DateTransformer: Transformer {
	case `default`

	var format: String {
		switch self {
		case .default:
			return "yyyy-MM-dd HH:mm:ss"
		}
	}

	func transform(_ decoded: String) throws -> Date {
		let formatter = DateFormatter()
		formatter.dateFormat = format
		formatter.timeZone = TimeZone(abbreviation: "UTC")

		guard let date = formatter.date(from: decoded) else {
			throw NSError(
				domain: "com.lykov.skyengtranslator",
				code: 0,
				userInfo: [NSLocalizedDescriptionKey: "Can't transform \(Input.self) to \(Output.self)"]
			)
		}

		return date
	}
}
