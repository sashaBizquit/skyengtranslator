//
//  ServerURLTransformer.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 08.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import Foundation

struct ServerURLTransformer: Transformer {

	static let shared = ServerURLTransformer()

	// MARK: Lifecycle

	private init() {}

	// MARK: Transformer

	func transform(_ decoded: String) throws -> URL? {
		let prefix = "https:"
		guard let url = URL(string: prefix + decoded) else {
			throw NSError(
				domain: "com.lykov.skyengtranslator",
				code: 0,
				userInfo: [NSLocalizedDescriptionKey: "Can't transform \(Input.self) to \(Output.self)"]
			)
		}
		return url
	}
}
