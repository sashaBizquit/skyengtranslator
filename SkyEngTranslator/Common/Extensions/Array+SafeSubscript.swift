//
//  Array+SafeSubscript.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 09.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

extension Array {
	subscript(safe index: Index) -> Element? {
		guard indices.contains(index) else {
			return nil
		}
		return self[index]
	}
}
