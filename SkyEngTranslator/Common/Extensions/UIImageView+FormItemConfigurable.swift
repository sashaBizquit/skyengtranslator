//
//  UIImageView+FormItemConfigurable.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 11.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import SDWebImage

extension UIImageView: FormItemConfigurable {
	func configure(with formItem: ImageFormItem) {
		switch formItem {
		case let .iconKey(key):
			image = key.image

		case .none:
			image = nil

		case let .url(url, placeholder):
			sd_setImage(with: url, placeholderImage: placeholder?.image)
		}
	}
}
