//
//  Encodable+Dictionary.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 08.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import Foundation

extension Encodable {
	var dictionary: [String: Any]? {
		guard let data = try? JSONEncoder().encode(self) else { return nil }
		return try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
	}
}
