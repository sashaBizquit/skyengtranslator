//
//  URLRequestComponents.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 07.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import Alamofire

typealias Parameters = Alamofire.Parameters

protocol URLRequestComponents: URLRequestConvertible {
	var baseURL: URL? { get }

	var method: HTTPMethod { get }

	var path: String { get }

	var parameters: Parameters? { get }
}

extension URLRequestComponents {
	var baseURL: URL? {
		URL(string: "https://dictionary.skyeng.ru/api/public/v1")
	}

	var method: HTTPMethod {
		.get
	}

	var parameters: Parameters? {
		nil
	}
}

extension URLRequestConvertible where Self: URLRequestComponents {
	func asURLRequest() throws -> URLRequest {
		guard var url = baseURL else  {
			assertionFailure("Remote url incorrect")
			return URLRequest(url: URL(fileURLWithPath: ""))
		}

		url.appendPathComponent(path)

		let encoding: ParameterEncoding
		switch method {
		case .post, .put:
			encoding = JSONEncoding.default

		default:
			encoding = URLEncoding(destination: .queryString)
		}

		let dataRequest = AF.request(url.absoluteString, method: method, parameters: parameters, encoding: encoding)

		return try dataRequest.convertible.asURLRequest()
	}
}
