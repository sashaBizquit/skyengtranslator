//
//  ApiServiceManagerAssembly.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 08.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

enum ApiServiceManagerAssembly: ManagerAssembly {
	static let manager: ApiServiceManagerProtocol = ApiServiceManager()
}
