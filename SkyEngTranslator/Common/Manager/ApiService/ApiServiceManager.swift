//
//  ApiServiceManager.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 07.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import Alamofire

struct ApiServiceManager {
	func performRequest<T: Decodable>(route: URLRequestComponents, completion: @escaping ResultCallback<T>) {
		let request = AF.request(route)

		request.validate()

		request.responseDecodable(of: T.self) { response in
			switch response.result {
			case .success(let data):
				completion(.success(data))

			case .failure(let error):
				completion(.failure(error))
			}
		}
	}
}
