//
//  MeaningsRequestModel.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 10.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import Foundation

struct MeaningsRequestModel: Encodable {
	let ids: [String]
	var updatedAt: Date?
}
