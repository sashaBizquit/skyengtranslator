//
//  MeaningsService.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 10.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

private enum MeaningsServiceRoutes: URLRequestComponents {
	case meanings(MeaningsRequestModel)

	var path: String {
		let servicePath = "/meanings"
		switch self {
		case .meanings:
			return servicePath
		}
	}

	var parameters: Parameters? {
		switch self {
		case .meanings(let requestModel):
			return requestModel.dictionary
		}
	}
}

extension ApiServiceManager: MeaningsServiceProtocol {
	func meanings(with requestModel: MeaningsRequestModel, completion: @escaping Self.MeaningsCallback) {
		performRequest(route: MeaningsServiceRoutes.meanings(requestModel), completion: completion)
	}
}
