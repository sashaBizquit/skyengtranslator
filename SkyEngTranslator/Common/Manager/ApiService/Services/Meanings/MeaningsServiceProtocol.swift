//
//  MeaningsServiceProtocol.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 10.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

protocol MeaningsServiceProtocol {
	typealias MeaningsCallback = ResultCallback<[WordMeaningModel]>

	func meanings(with requestModel: MeaningsRequestModel, completion: @escaping MeaningsCallback)
}
