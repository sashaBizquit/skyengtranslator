//
//  WordsService.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 08.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

private enum WordsServiceRoutes: URLRequestComponents {
	case search(SearchWordsRequestModel)

	var path: String {
		let servicePath = "/words"
		switch self {
		case .search:
			return servicePath + "/search"
		}
	}

	var parameters: Parameters? {
		switch self {
		case .search(let requestModel):
			return requestModel.dictionary
		}
	}
}

extension ApiServiceManager: WordsServiceProtocol {
	func search(with requestModel: SearchWordsRequestModel, completion: @escaping Self.SearchCallback) {
		performRequest(route: WordsServiceRoutes.search(requestModel), completion: completion)
	}
}
