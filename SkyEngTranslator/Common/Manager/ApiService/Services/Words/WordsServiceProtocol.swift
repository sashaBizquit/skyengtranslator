//
//  WordsServiceProtocol.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 07.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

protocol WordsServiceProtocol {
	typealias SearchCallback = ResultCallback<[SearchAnswerModel]>

	func search(with requestModel: SearchWordsRequestModel, completion: @escaping SearchCallback)
}
