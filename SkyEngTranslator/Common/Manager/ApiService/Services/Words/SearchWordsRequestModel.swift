//
//  SearchWordsRequestModel.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 07.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

struct SearchWordsRequestModel: Encodable {
	let search: String
	let page: Int
	let pageSize: Int
}
