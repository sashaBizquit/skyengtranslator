//
//  ApiServiceManagerMock.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 11.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

struct ApiServiceManagerMock: ApiServiceManagerProtocol {
	func search(with requestModel: SearchWordsRequestModel, completion: @escaping WordsServiceProtocol.SearchCallback) {
		completion(.success([]))
	}

	func meanings(with requestModel: MeaningsRequestModel, completion: @escaping MeaningsServiceProtocol.MeaningsCallback) {
		completion(.success([]))
	}
}
