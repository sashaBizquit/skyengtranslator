//
//  Typealiases.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 07.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

typealias VoidCallback = () -> Void
typealias Callback<T> = (T) -> Void
typealias Provider<T> = () -> T
typealias ResultCallback<T> = Callback<Result<T, Error>>
typealias VoidResultCallback = ResultCallback<Void>
