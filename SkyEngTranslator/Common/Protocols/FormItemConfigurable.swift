//
//  FormItemConfigurable.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 08.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import UIKit

protocol FormItemConfigurable {
	associatedtype FormItem

	func configure(with formItem: FormItem)
}
