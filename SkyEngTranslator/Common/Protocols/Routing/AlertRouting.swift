//
//  AlertRouting.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 08.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import UIKit

protocol AlertRouting: RouterProtocol {
	func showAlert(title: String?, subtitle: String?, defaultButton: String?)
}

extension AlertRouting {
	func showAlert(title: String? = nil, subtitle: String? = nil, defaultButton: String? = nil) {
		let localization = Localization.Alert.self
		let alertTitle = title ?? localization.error.localized
		let alertViewController = UIAlertController(title: alertTitle, message: subtitle, preferredStyle: .alert)
		let actionTitle = defaultButton ?? localization.ok.localized.uppercased()
		let defaultAction = UIAlertAction(title: actionTitle, style: .default, handler: nil)
		alertViewController.addAction(defaultAction)
		showViewController(viewController: alertViewController, type: .present)
	}
}
