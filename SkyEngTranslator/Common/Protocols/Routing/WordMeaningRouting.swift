//
//  WordMeaningRouting.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 09.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

protocol WordMeaningRouting: RouterProtocol {
	var wordMeaningFactory: WordMeaningAssembly.Factory { get }

	func showWordMeaningModule(with inputModel: WordMeaningInputModel)
}

extension WordMeaningRouting {
	func showWordMeaningModule(with inputModel: WordMeaningInputModel) {
		showModule(providedBy: wordMeaningFactory, type: .push) { presenter in
			presenter.setup(with: inputModel)
		}
	}
}
