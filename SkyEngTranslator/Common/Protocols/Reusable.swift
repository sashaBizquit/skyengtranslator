//
//  Reusable.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 08.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import UIKit

/// Протокол переиспользуемой сущности.
protocol Reusable: AnyObject {
	static var reuseIdentifier: String { get }
}

extension Reusable {
	static var reuseIdentifier: String {
		"\(self)"
	}
}

extension UITableViewCell: Reusable {}

extension UITableView {
	func registerReusable<T: Reusable>(_ reusable: T.Type) {
		register(T.self, forCellReuseIdentifier: reusable.reuseIdentifier)
	}

	func dequeueReusableCell<T: UITableViewCell>(withReusable reusable: T.Type, for indexPath: IndexPath) -> T? {
		return dequeueReusableCell(withIdentifier: reusable.reuseIdentifier, for: indexPath) as? T
	}
}
