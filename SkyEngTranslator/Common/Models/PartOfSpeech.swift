//
//  PartOfSpeech.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 07.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

/// Часть речи.
enum PartOfSpeech: String, Decodable {

	/// Существительное.
	case noun

	/// Местоимение.
	case pronoun

	/// Глагол.
	case verb

	/// Наречие.
	case adverb

	/// Числительное.
	case numeral

	/// Прилагательное.
	case adjective

	/// Фраза.
	case phrase

	/// Аббревиатура.
	case abbreviation

	/// Предложение.
	case preposition

	/// Союз.
	case conjunction

	/// Междометие.
	case interjection

	/// Предложение.
	case article

	/// Частица.
	case particle

	/// Идиома.
	case idiom

	/// NAN
	case undefined

	init?(rawValue: String) {
		switch rawValue {
		case "n":
			self = .noun

		case "prn":
			self = .pronoun

		case "v", "md":
			self = .verb

		case "r":
			self = .adverb

		case "crd", "ord":
			self = .numeral

		case "j":
			self = .adjective

		case "ph":
			self = .phrase

		case "abb":
			self = .abbreviation

		case "prp":
			self = .preposition

		case "cjc":
			self = .conjunction

		case "exc":
			self = .interjection

		case "det":
			self = .article

		case "x":
			self = .particle

		case "phi":
			self = .idiom

		default:
			self = .undefined
		}
	}
}
