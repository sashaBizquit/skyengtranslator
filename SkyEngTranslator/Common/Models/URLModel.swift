//
//  URLModel.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 10.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import Foundation

struct URLModel: Decodable {
	enum CodingKeys: String, CodingKey {
		case url
	}

	let url: URL?

	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		url = try container.decode(.url, transformer: ServerURLTransformer.shared)
	}
}
