//
//  SearchAnswerModel.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 07.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

struct SearchAnswerModel: Decodable {
	let id: Int
	let text: String
	let meanings: [SearchMeaningModel]
}
