//
//  SearchMeaningModel.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 07.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import Foundation

struct SearchMeaningModel: Decodable {
	enum CodingKeys: String, CodingKey {
		case id
		case partOfSpeech = "partOfSpeechCode"
		case translation
		case transcription
		case soundURL = "soundUrl"
		case imageURL = "imageUrl"
		case previewURL = "previewUrl"
	}

	let id: Int
	let partOfSpeech: PartOfSpeech
	let translation: TranslationModel
	let transcription: String?
	let soundURL: URL?
	let imageURL: URL?
	let previewURL: URL?

	init(
		id: Int,
		partOfSpeech: PartOfSpeech,
		translation: TranslationModel,
		transcription: String?,
		soundURL: URL?,
		imageURL: URL?,
		previewURL: URL?
	) {
		self.id = id
		self.partOfSpeech = partOfSpeech
		self.translation = translation
		self.transcription = transcription
		self.soundURL = soundURL
		self.imageURL = imageURL
		self.previewURL = previewURL
	}

	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		id = try container.decode(.id)
		partOfSpeech = try container.decode(.partOfSpeech)
		translation = try container.decode(.translation)
		transcription = try container.decode(.transcription)
		soundURL = try container.decode(.soundURL, transformer: ServerURLTransformer.shared)
		imageURL = try container.decode(.imageURL, transformer: ServerURLTransformer.shared)
		previewURL = try container.decode(.previewURL, transformer: ServerURLTransformer.shared)
	}
}
