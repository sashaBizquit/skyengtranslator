//
//  TranslationModel.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 08.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

struct TranslationModel: Decodable {
	let text: String
	let note: String?
}
