//
//  WordMeaningModel.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 10.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import Foundation

struct WordMeaningModel: Decodable {
	enum CodingKeys: String, CodingKey {
		case id
		case wordId
		case difficultyLevel
		case partOfSpeech = "partOfSpeechCode"
		case prefix
		case text
		case soundURL = "soundUrl"
		case transcription
		case translation
		case updatedAt
		case images
	}

	let id: String
	let wordId: Int
	let difficultyLevel: Int?
	let partOfSpeech: PartOfSpeech
	let prefix: String?
	let text: String
	let soundURL: URL?
	let transcription: String
	let translation: TranslationModel
	let updatedAt: Date
	let images: [URLModel]

	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		id = try container.decode(.id)
		wordId = try container.decode(.wordId)
		difficultyLevel = try container.decodeIfPresent(.difficultyLevel)
		partOfSpeech = try container.decode(.partOfSpeech)
		prefix = try container.decodeIfPresent(.prefix)
		text = try container.decode(.text)
		soundURL = try container.decode(.soundURL, transformer: ServerURLTransformer.shared)
		transcription = try container.decode(.transcription)
		translation = try container.decode(.translation)
		updatedAt = try container.decode(.updatedAt, transformer: DateTransformer.default)
		images = try container.decode(.images)
	}
}
