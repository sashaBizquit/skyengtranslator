//
//  Palette.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 07.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import UIKit

enum Palette {
	enum Blue {
		static let light = UIColor(withRed: 141, green: 226, blue: 252)
		static let intense = UIColor(withRed: 86, green: 191, blue: 249)
	}

	enum Gray {
		static let dark = UIColor(withRed: 40, green: 40, blue: 40)
	}

	static let clear: UIColor = .clear
	static let white: UIColor = .white
	static let black = UIColor(withRed: 0, green: 0, blue: 20)
}

extension UIColor {
	convenience init(withRed red: UInt8, green: UInt8, blue: UInt8, alpha: CGFloat = 1) {
		self.init(red: CGFloat(red)/255, green: CGFloat(green)/255, blue: CGFloat(blue)/255, alpha: alpha)
	}
}
