//
//  MeaningTableViewCell.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 08.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import SnapKit

/// Ячейка перевода.
final class MeaningTableViewCell: UITableViewCell {

	/// Картинка, сопровождающая перевод.
	let iconImageView = UIImageView()

	/// Текст варианта перевода.
	let meaningLabel = UILabel()

	/// Текст уточнения.
	let noteLabel = UILabel()

	/// Стэк содержимого ячейки.
	private let contentStackView = UIStackView()

	/// Стэк текстов.
	private let labelsStackView = UIStackView()

	// MARK: Lifecycle

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setupUI()
	}

	required init?(coder: NSCoder) {
		assertionFailure("init(coder:) has not been implemented")
		return nil
	}

	// MARK: Private

	private func setupUI() {
		setupContentStackView()
		setupIconImageView()
		setupLabelsStackView()
		setupMeaningLabel()
		setupNoteLabel()
		setupSelectedBackgroundView()
	}

	private func setupContentStackView() {
		contentView.addSubview(contentStackView)

		contentStackView.snp.makeConstraints { make in
			make.edges.equalToSuperview().inset(16)
		}

		contentStackView.axis = .horizontal
		contentStackView.spacing = 16
		contentStackView.alignment = .center
	}

	private func setupIconImageView() {
		contentStackView.addArrangedSubview(iconImageView)

		iconImageView.snp.makeConstraints { make in
			make.size.equalTo(36)
		}

		iconImageView.contentMode = .scaleAspectFill
		iconImageView.clipsToBounds = true
		iconImageView.layer.cornerRadius = 5
	}

	private func setupLabelsStackView() {
		contentStackView.addArrangedSubview(labelsStackView)

		labelsStackView.axis = .vertical
		labelsStackView.alignment = .leading
		labelsStackView.spacing = 4
		labelsStackView.setContentHuggingPriority(.defaultLow, for: .horizontal)
	}

	private func setupMeaningLabel() {
		labelsStackView.addArrangedSubview(meaningLabel)

		meaningLabel.textColor = Palette.black
		meaningLabel.font = .systemFont(ofSize: 16, weight: .medium)
	}

	private func setupNoteLabel() {
		labelsStackView.addArrangedSubview(noteLabel)

		noteLabel.textColor = Palette.Gray.dark
		noteLabel.font = .systemFont(ofSize: 14, weight: .regular)
	}

	private func setupSelectedBackgroundView() {
		let selectedView = UIView()
		selectedView.backgroundColor = Palette.Blue.light
		selectedBackgroundView = selectedView
	}
}
