//
//  MeaningTableViewCell+FormItemConfigurable.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 08.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

extension MeaningTableViewCell: FormItemConfigurable {
	struct FormItem {
		let iconFormItem: ImageFormItem
		let meaning: String
		let note: String?
	}

	func configure(with formItem: FormItem) {
		iconImageView.configure(with: formItem.iconFormItem)
		meaningLabel.text = formItem.meaning
		noteLabel.text = formItem.note
	}
}
