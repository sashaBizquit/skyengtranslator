//
//  Localization.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 07.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

enum Localization {
	enum PartOfSpeech: LocalizationProvider {
		case noun
		case pronoun
		case verb
		case adverb
		case numeral
		case adjective
		case phrase
		case abbreviation
		case preposition
		case conjunction
		case interjection
		case article
		case particle
		case idiom
	}

	enum Search: LocalizationProvider {
		case title
		case searchPlaceholder
	}

	enum WordMeaning: LocalizationProvider {
		case title
		case word
		case meaning
	}

	enum Alert: LocalizationProvider {
		case error
		case ok
	}
}
