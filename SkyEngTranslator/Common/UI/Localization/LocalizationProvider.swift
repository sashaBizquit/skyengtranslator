//
//  LocalizationProvider.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 11.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import Foundation

protocol LocalizationProvider {
	var localized: String { get }
	var key: String { get }
}

extension LocalizationProvider {
	var localized: String {
		NSLocalizedString(key, comment: "")
	}

	var key: String {
		"\(type(of: self))_\(self)"
	}
}
