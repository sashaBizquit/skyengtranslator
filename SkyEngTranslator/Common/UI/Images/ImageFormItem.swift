//
//  ImageFormItem.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 08.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import Foundation

/// Модель любого изображения в приложении.
enum ImageFormItem {

	/// Иконка хранящаяся в приложении
	case iconKey(ImageAssetKey)

	/// Скачиваемое изображение
	case url(_: URL, placeholder: ImageAssetKey?)

	/// Отсутствие изображения.
	case none
}
