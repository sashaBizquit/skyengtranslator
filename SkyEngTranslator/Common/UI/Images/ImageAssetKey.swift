//
//  ImageAssetKey.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 08.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import UIKit

enum ImageAssetKey: String {

	case skyengLogo

	var image: UIImage {
		guard let image = UIImage(named: rawValue) else {
			assertionFailure()
			return UIImage()
		}
		return image
	}
}
