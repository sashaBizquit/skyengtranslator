//
//  BaseModuleViewController.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 07.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import UIKit

class BaseModuleViewController<PresenterType: PresenterProtocol>: UIViewController, ModuleViewProtocol {
	typealias Presenter = PresenterType

	let presenter: Presenter

	required init(presenter: Presenter) {
		self.presenter = presenter

		super.init(nibName: nil, bundle: nil)
		
		presenter.router.view = self
	}

	required init?(coder: NSCoder) {
		assertionFailure("init(coder:) has not been implemented")
		return nil
	}
}
