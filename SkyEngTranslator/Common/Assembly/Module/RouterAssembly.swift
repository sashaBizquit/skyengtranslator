//
//  RouterAssembly.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 07.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import UIKit

enum RouterTransitionType {
	case present
	case push
}

protocol RouterProtocol: AnyObject {
	var view: UIViewController? { get set }
}

extension RouterProtocol {
	func showModule<T: ModuleViewProtocol>(
		providedBy factory: ModuleFactory<T>,
		type: RouterTransitionType,
		animated isAnimated: Bool = true,
		configure: Callback<T.Presenter>? = nil
	) {
		guard view != nil else { return }

		let module = factory()
		configure?(module.presenter)

		showViewController(viewController: module, type: type, animated: isAnimated)
	}

	func showViewController<T: UIViewController>(
		viewController: T,
		type: RouterTransitionType,
		animated isAnimated: Bool = true
	) {
		switch type {
		case .present:
			view?.present(viewController, animated: isAnimated)

		case .push:
			view?.navigationController?.pushViewController(viewController, animated: isAnimated)
		}
	}
}

protocol RouterAssembly {
	associatedtype Router: RouterProtocol

	static var router: Router { get }
}
