//
//  PresenterAssembly.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 07.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

protocol PresenterProtocol: AnyObject {
	associatedtype Router: RouterProtocol

	var router: Router { get }
}

protocol PresenterAssembly {
	associatedtype Presenter: PresenterProtocol

	static var presenter: Presenter { get }
}
