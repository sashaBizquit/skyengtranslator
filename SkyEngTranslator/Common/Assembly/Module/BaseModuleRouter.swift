//
//  BaseModuleRouter.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 11.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import UIKit

class BaseModuleRouter: RouterProtocol {
	weak var view: UIViewController?
}
