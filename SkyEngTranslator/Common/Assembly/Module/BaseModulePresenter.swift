//
//  BaseModulePresenter.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 11.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

class BaseModulePresenter<RouterType: RouterProtocol>: PresenterProtocol {
	typealias Router = RouterType

	let router: RouterType

	init(router: RouterType) {
		self.router = router
	}
}
