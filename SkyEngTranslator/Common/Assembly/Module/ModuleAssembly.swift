//
//  ModuleAssembly.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 07.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import UIKit

typealias ModuleFactory<T: ModuleViewProtocol> = Provider<T>

protocol ModuleViewProtocol: UIViewController {
	associatedtype Presenter: PresenterProtocol

	var presenter: Presenter { get }

	init(presenter: Presenter)
}

protocol ModuleAssembly: PresenterAssembly, RouterAssembly {
	associatedtype View: ModuleViewProtocol where View.Presenter == Presenter, Presenter.Router == Router

	typealias Factory = ModuleFactory<View>

	static var view: View { get }
}

extension ModuleAssembly {

	static var factory: Factory {
		{ Self.view }
	}
}
