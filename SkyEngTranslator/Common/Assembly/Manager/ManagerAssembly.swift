//
//  ManagerAssembly.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 08.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

protocol ManagerAssembly {
	associatedtype Manager

	static var manager: Manager { get }
}
