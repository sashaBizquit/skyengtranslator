//
//  SearchPresenterProtocol.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 06.10.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import Foundation

protocol SearchPresenterProtocol: PresenterProtocol {
	typealias TextUpdateResultCallback = ResultCallback<Bool>

	var numberOfSections: Int { get }
	var isInfinityScrollAvaliable: Bool { get }

	func numberOfItems(in section: Int) -> Int
	func title(for section: Int) -> String?
	func item(at indexPath: IndexPath) -> SearchTableFormItem.RowItem?
	func didSelectItem(at indexPath: IndexPath)
	func updateNextPage(completion: @escaping TextUpdateResultCallback)
	func didChangeSearchText(_ text: String, completion: @escaping TextUpdateResultCallback)
}
