//
//  SearchPresenter.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 07.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import Foundation

struct SearchTableFormItem {
	struct SectionItem {
		var title: String
		var rows: [RowItem] = []
	}

	enum RowItem {
		/// Ячейка со значением
		case meaning(MeaningTableViewCell.FormItem)
	}

	var sections: [SectionItem] = []
}

private enum Constants {
	static let pageSize: Int = 20
	static let pagesStartIndex: Int = 1
	static let searchDelay: Double = 1
}

final class SearchPresenter<Router: SearchRouterProtocol>: BaseModulePresenter<Router>, SearchPresenterProtocol {

	// MARK: Private properties

	private let apiService: WordsServiceProtocol
	private var tableFormItem = SearchTableFormItem(sections: [])
	private var answerModels: [SearchAnswerModel] = []

	/// Номер следуюдий страницы в случае прогрузки (infinity scroll).
	private var nextPageIndex: Int = Constants.pagesStartIndex

	/// Таймер отложенной загрузки данных.
	private var timer: Timer?

	/// Загружаются ли сейчас данные.
	private var isLoading = false

	/// Текст поискового запроса.
	private var searchText: String = ""

	// MARK: Internal
	/// Доступна ли прогрузка (infinity scroll).
	private(set) var isInfinityScrollAvaliable: Bool = true

	private var sections: [SearchTableFormItem.SectionItem] {
		tableFormItem.sections
	}

	// MARK: Lifecycle

	init(router: Router, apiService: WordsServiceProtocol) {
		self.apiService = apiService
		super.init(router: router)
	}

	// MARK: - SearchPresenterProtocol

	var numberOfSections: Int {
		sections.count
	}

	func numberOfItems(in section: Int) -> Int {
		return sectionItem(for: section)?.rows.count ?? 0
	}

	func title(for section: Int) -> String? {
		return sectionItem(for: section)?.title
	}

	func item(at indexPath: IndexPath) -> SearchTableFormItem.RowItem? {
		return sectionItem(for: indexPath.section)?.rows[safe: indexPath.row]
	}

	func didSelectItem(at indexPath: IndexPath) {
		let section = indexPath.section
		let model = answerModels[safe: section]?.meanings[safe: indexPath.row]
		let word = sectionItem(for: section)?.title
		let inputModel = WordMeaningInputModel(word: word, meaning: model)
		router.showWordMeaningModule(with: inputModel)
	}

	func updateNextPage(completion: @escaping TextUpdateResultCallback) {
		updateSearch(completion: completion)
	}

	func didChangeSearchText(_ text: String, completion: @escaping TextUpdateResultCallback) {
		guard searchText != text else { return completion(.success(false)) }

		searchText = text

		guard !text.isEmpty else {
			clearItems()
			return completion(.success(true))
		}

		invalidateTimer()

		/// Ставим задержку в поиске, чтобы запрос не проходил,
		/// если за ним в течение Constants.searchDelay секунд последует другой.
		timer = Timer.scheduledTimer(withTimeInterval: Constants.searchDelay, repeats: false) { [weak self] _ in
			guard let self = self else { return completion(.success(false)) }
			self.clearItems()
			self.updateSearch(completion: completion)
		}
	}

	// MARK: Private

	private func sectionItem(for section: Int) -> SearchTableFormItem.SectionItem? {
		return sections[safe: section]
	}

	private func updateSearch(completion: @escaping TextUpdateResultCallback) {
		/// Если все-таки ожидается ответ на предыдущий запрос, не делаем новый.
		guard !isLoading else { return }

		/// Если текста запроса нет, возвращаем в исходное состояние.
		guard !searchText.isEmpty else {
			clearItems()
			return completion(.success(true))
		}

		let pageIndex = nextPageIndex
		let requestModel = SearchWordsRequestModel(search: searchText, page: pageIndex, pageSize: Constants.pageSize)
		isLoading = true
		apiService.search(with: requestModel) { [weak self] result in
			guard let self = self, requestModel.search == self.searchText else { return completion(.success(false)) }

			self.isLoading = false

			switch result {
			case let .success(searchModels):
				self.nextPageIndex = pageIndex + 1
				self.updateItems(with: searchModels, searchModel: requestModel)
				completion(.success(true))

			case let .failure(error):
				self.router.showAlert(subtitle: error.localizedDescription)
				completion(.failure(error))
			}
		}
	}

	private func updateItems(with models: [SearchAnswerModel], searchModel: SearchWordsRequestModel) {
		let search = searchModel.search
		guard !search.isEmpty else { return clearItems() }

		let page = searchModel.page
		let pageSize = searchModel.pageSize
		let expectedCurrentSize = (page - 1) * pageSize

		/// Если текущие данные не соответствуют размерности ответа - отключаем infinity scroll
		guard sections.count == expectedCurrentSize else {
			isInfinityScrollAvaliable = false
			return
		}

		var newSections: [SearchTableFormItem.SectionItem] = models.map { model in
			var sectionItem = SearchTableFormItem.SectionItem(title: model.text)
			sectionItem.rows = model.meanings.map { meaning in
				let iconFormItem: ImageFormItem
				if let previewUrl = meaning.previewURL {
					iconFormItem = .url(previewUrl, placeholder: nil)
				} else {
					iconFormItem = .none
				}

				let translation = meaning.translation
				let formItem = MeaningTableViewCell.FormItem(
					iconFormItem: iconFormItem,
					meaning: translation.text,
					note: translation.note
				)
				return .meaning(formItem)
			}
			return sectionItem
		}
		let count = newSections.count

		/// Если кол-во пришедших элементов меньше размера страницы - отключаем infinity scroll
		isInfinityScrollAvaliable = count >= pageSize

		if count > pageSize {
			newSections.removeSubrange(pageSize..<count)
		}

		tableFormItem.sections.append(contentsOf: newSections)
		answerModels.append(contentsOf: models)
	}

	private func clearItems() {
		answerModels.removeAll()
		tableFormItem.sections.removeAll()
		nextPageIndex = Constants.pagesStartIndex
		isInfinityScrollAvaliable = true
		isLoading = false
		invalidateTimer()
	}

	private func invalidateTimer() {
		timer?.invalidate()
		timer = nil
	}
}
