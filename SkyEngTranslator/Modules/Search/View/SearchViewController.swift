//
//  SearchViewController.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 07.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import SnapKit

final class SearchViewController<Presenter: SearchPresenterProtocol>:
	BaseModuleViewController<Presenter>,
	UISearchResultsUpdating,
	UITableViewDataSource,
	UITableViewDelegate
{

	// MARK: UI components

	private lazy var searchController = UISearchController(searchResultsController: nil)
	private lazy var searchTableView = UITableView()
	private lazy var spinner = UIActivityIndicatorView()

	private lazy var defaultPresenterCallback: Presenter.TextUpdateResultCallback = { [weak self] result in
		guard let self = self else { return }

		DispatchQueue.main.async {
			self.spinner.stopAnimating()

			switch result {
			case let .success(didUpdate):
				if didUpdate {
					self.searchTableView.reloadData()
				}

			case .failure:
				break
			}
		}
	}

	private var searchBar: UISearchBar {
		searchController.searchBar
	}

	// MARK: Lifecycle

	override func loadView() {
		view = UIView()
		setupUI()
	}

	// MARK: - UISearchResultsUpdating

	func updateSearchResults(for searchController: UISearchController) {
		spinner.startAnimating()
		presenter.didChangeSearchText(searchBar.text ?? "", completion: defaultPresenterCallback)
	}

	// MARK: - UITableViewDataSource

	func numberOfSections(in tableView: UITableView) -> Int {
		return presenter.numberOfSections
	}

	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return presenter.title(for: section)
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return presenter.numberOfItems(in: section)
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withReusable: MeaningTableViewCell.self, for: indexPath) else {
			assertionFailure()
			return UITableViewCell()
		}
		switch presenter.item(at: indexPath) {
		case .meaning(let meaningCellFormItem):
			cell.configure(with: meaningCellFormItem)

		default:
			assertionFailure()
			break
		}
		return cell
	}

	// MARK: - UITableViewDelegate

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		searchBar.endEditing(true)

		presenter.didSelectItem(at: indexPath)
	}

	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		let sections = numberOfSections(in: tableView)
		// Если собиаремся показать последнюю секцию непустой таблицы и прогрузка возможна, ...
		guard sections > 0,
			indexPath.section + 1 == sections,
			presenter.isInfinityScrollAvaliable
		else {
			return
		}

		// ... показываем спиннер и начинаем прогрузку следующей страницы.
		spinner.startAnimating()
		presenter.updateNextPage(completion: defaultPresenterCallback)
	}

	// MARK: UIScrollViewDelegate

	func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
		// Если начали скролл - прячем клавиатуру.
		searchBar.endEditing(true)
	}

	// MARK: - Private

	private func setupUI() {
		setupView()
		setupNavigationItem()
		setupSearchController()
		setupSearchTableView()
		setupSpinner()
	}

	private func setupView() {
		view.backgroundColor = Palette.white
	}

	private func setupNavigationItem() {
		navigationItem.title = Localization.Search.title.localized
	}

	private func setupSearchController() {
		navigationItem.searchController = searchController

		searchController.searchResultsUpdater = self
		searchController.obscuresBackgroundDuringPresentation = false
		searchController.hidesNavigationBarDuringPresentation = false

		searchBar.placeholder = Localization.Search.searchPlaceholder.localized
		searchBar.tintColor = Palette.Blue.intense
	}

	private func setupSearchTableView() {
		view.addSubview(searchTableView)

		searchTableView.snp.makeConstraints { make in
			make.top.leading.trailing.equalTo(view.safeAreaLayoutGuide)
			make.bottom.equalToSuperview()
		}

		searchTableView.dataSource = self
		searchTableView.delegate = self
		searchTableView.separatorStyle = .none
		searchTableView.alwaysBounceVertical = true
		searchTableView.registerReusable(MeaningTableViewCell.self)
	}

	private func setupSpinner() {
		navigationItem.rightBarButtonItem = UIBarButtonItem(customView: spinner)
		spinner.color = Palette.Blue.intense
	}
}
