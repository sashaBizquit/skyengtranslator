//
//  SearchAssembly.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 07.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

enum SearchAssembly: ModuleAssembly {
	typealias View = SearchViewController<Presenter>
	typealias Presenter = SearchPresenter<Router>
	typealias Router = SearchRouter

	static var view: View {
		SearchViewController(presenter: presenter)
	}

	static var presenter: Presenter {
		SearchPresenter(router: router, apiService: ApiServiceManagerAssembly.manager)
	}

	static var router: Router {
		SearchRouter(wordMeaningFactory: WordMeaningAssembly.factory)
	}
}
