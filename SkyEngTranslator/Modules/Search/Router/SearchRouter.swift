//
//  SearchRouter.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 07.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

final class SearchRouter: BaseModuleRouter, SearchRouterProtocol {
	let wordMeaningFactory: WordMeaningAssembly.Factory

	init(wordMeaningFactory: @escaping WordMeaningAssembly.Factory) {
		self.wordMeaningFactory = wordMeaningFactory
	}
}
