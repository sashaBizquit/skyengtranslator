//
//  SearchRouterProtocol.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 06.10.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

typealias SearchRouterProtocol = AlertRouting & WordMeaningRouting
