//
//  WordMeaningInputProtocol.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 10.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

struct WordMeaningInputModel {
	let word: String?
	let meaning: SearchMeaningModel?
}

protocol WordMeaningInputProtocol {
	func setup(with inputModel: WordMeaningInputModel)
}
