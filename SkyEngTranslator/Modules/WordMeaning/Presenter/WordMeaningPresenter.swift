//
//  WordMeaningPresenter.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 09.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

final class WordMeaningPresenter<Router: WordMeaningRouterProtocol>:
	BaseModulePresenter<Router>,
	WordMeaningPresenterProtocol,
	WordMeaningInputProtocol
{
	private let apiService: MeaningsServiceProtocol
	private var searchMeaning: SearchMeaningModel?
	private var wordMeaning: WordMeaningModel?
	private var inputWord: String?

	private var translation: TranslationModel? {
		wordMeaning?.translation ?? searchMeaning?.translation
	}

	// MARK: Lifecycle

	init(router: Router, apiService: MeaningsServiceProtocol) {
		self.apiService = apiService
		super.init(router: router)
	}

	// MARK: Internal

	var imageFormItem: ImageFormItem {
		guard let url = searchMeaning?.imageURL else { return .none }
		return .url(url, placeholder: nil)
	}

	var title: String? {
		inputWord ?? wordMeaning?.text
	}

	var titleHeader: String? {
		title == nil ? nil : Localization.WordMeaning.word.localized + ":"
	}

	var subtitle: String? {
		translation?.text
	}

	var subtitleHeader: String? {
		subtitle == nil ? nil : Localization.WordMeaning.meaning.localized + ":"
	}

	func updateWordMeaning(completion: @escaping UpdateWordMeaningResultCallback) {
		guard let id = searchMeaning?.id else { return }
		let requestModel = MeaningsRequestModel(ids: ["\(id)"])
		apiService.meanings(with: requestModel) { [weak self] result in
			guard let self = self else { return }
			switch result {
			case let .success(models):
				self.wordMeaning = models.first

			case let .failure(error):
				self.router.showAlert(subtitle: error.localizedDescription)
			}

			completion(result.map { _ in })
		}
	}

	// MARK: - WordMeaningInputProtocol

	func setup(with inputModel: WordMeaningInputModel) {
		searchMeaning = inputModel.meaning
		inputWord = inputModel.word
	}
}
