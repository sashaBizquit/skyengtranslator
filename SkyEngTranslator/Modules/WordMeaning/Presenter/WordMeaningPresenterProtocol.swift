//
//  WordMeaningPresenterProtocol.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 11.10.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

protocol WordMeaningPresenterProtocol: PresenterProtocol {
	typealias UpdateWordMeaningResultCallback = ResultCallback<Void>

	var imageFormItem: ImageFormItem { get }
	var title: String? { get }
	var titleHeader: String? { get }
	var subtitle: String? { get }
	var subtitleHeader: String? { get }

	func updateWordMeaning(completion: @escaping UpdateWordMeaningResultCallback)
}
