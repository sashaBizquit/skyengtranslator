//
//  WordMeaningViewController.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 09.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import UIKit

final class WordMeaningViewController<Presenter: WordMeaningPresenterProtocol>: BaseModuleViewController<Presenter>, UIScrollViewDelegate {

	private lazy var spinner = UIActivityIndicatorView()
	private lazy var scrollView = UIScrollView()
	private lazy var contentView = UIView()
	private lazy var imageView = UIImageView()
	private lazy var titlesStackView = UIStackView()
	private lazy var titleHeaderLabel = UILabel()
	private lazy var titleLabel = UILabel()
	private lazy var subtitleHeaderLabel = UILabel()
	private lazy var subtitleLabel = UILabel()

	// MARK: Lifecycle

	override func loadView() {
		view = UIView()
		setupUI()
	}

	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		scrollView.contentSize = contentView.bounds.size
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		updateUI()
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		reloadData()
	}

	// MARK: - UIScrollViewDelegate

	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		scrollView.contentOffset.x = 0
	}

	// MARK: Private

	private func setupUI() {
		setupView()
		setupNavigationItem()
		setupScrollView()
		setupContentView()
		setupImageView()
		setupTitlesStackView()
		setupTitleHeader()
		setupTitleLabel()
		setupSubtitleHeader()
		setupSubtitleLabel()
	}

	private func setupView() {
		view.backgroundColor = Palette.white
	}

	private func setupNavigationItem() {
		navigationItem.title = Localization.WordMeaning.title.localized

		navigationItem.rightBarButtonItem = UIBarButtonItem(customView: spinner)
		spinner.color = Palette.Blue.intense
	}

	private func setupScrollView() {
		view.addSubview(scrollView)

		scrollView.snp.makeConstraints { make in
			make.top.left.right.equalTo(view.safeAreaLayoutGuide)
			make.bottom.equalToSuperview()
		}

		scrollView.alwaysBounceVertical = false
		scrollView.showsHorizontalScrollIndicator = false
		scrollView.contentInsetAdjustmentBehavior = .always
		scrollView.delegate = self
	}

	private func setupContentView() {
		scrollView.addSubview(contentView)

		contentView.snp.makeConstraints { make in
			make.edges.equalToSuperview()
		}
	}

	private func setupImageView() {
		contentView.addSubview(imageView)

		imageView.snp.makeConstraints { make in
			make.left.top.equalToSuperview().inset(16)
			make.height.equalTo(imageView.snp.width)
			make.right.equalTo(view.snp.centerX).offset(-16)
			make.bottom.equalToSuperview().offset(-16)
		}

		imageView.contentMode = .scaleAspectFill
		imageView.clipsToBounds = true
		imageView.layer.cornerRadius = 30
	}

	private func setupTitlesStackView() {
		contentView.addSubview(titlesStackView)

		titlesStackView.snp.makeConstraints { make in
			make.centerY.equalTo(imageView)
			make.left.equalTo(imageView.snp.right).offset(32)
			make.top.greaterThanOrEqualToSuperview().offset(16)
			make.right.equalToSuperview()
		}

		titlesStackView.axis = .vertical
		titlesStackView.alignment = .leading
		titlesStackView.spacing = 8
	}

	private func setupTitleHeader() {
		titlesStackView.addArrangedSubview(titleHeaderLabel)

		titleHeaderLabel.textColor = Palette.black
		titleHeaderLabel.font = .systemFont(ofSize: 16, weight: .bold)
		titleHeaderLabel.numberOfLines = 1
	}

	private func setupTitleLabel() {
		titlesStackView.addArrangedSubview(titleLabel)

		titleLabel.textColor = Palette.black
		titleLabel.font = .systemFont(ofSize: 16, weight: .regular)
		titleLabel.numberOfLines = 3
	}

	private func setupSubtitleHeader() {
		titlesStackView.addArrangedSubview(subtitleHeaderLabel)

		subtitleHeaderLabel.textColor = Palette.black
		subtitleHeaderLabel.font = .systemFont(ofSize: 16, weight: .bold)
		subtitleHeaderLabel.numberOfLines = 1
	}

	private func setupSubtitleLabel() {
		titlesStackView.addArrangedSubview(subtitleLabel)

		subtitleLabel.textColor = Palette.Gray.dark
		subtitleLabel.font = .systemFont(ofSize: 16, weight: .regular)
		subtitleLabel.numberOfLines = 4
		subtitleLabel.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
	}

	private func updateUI() {
		imageView.configure(with: presenter.imageFormItem)
		titleLabel.hidingText = presenter.title
		titleHeaderLabel.hidingText = presenter.titleHeader
		subtitleLabel.hidingText = presenter.subtitle
		subtitleHeaderLabel.hidingText = presenter.subtitleHeader
	}

	private func reloadData() {
		spinner.startAnimating()
		presenter.updateWordMeaning { [weak self] result in
			guard let self = self else { return }
			DispatchQueue.main.async {
				self.spinner.stopAnimating()

				switch result {
				case .success:
					self.updateUI()

				case .failure:
					break
				}
			}
		}
	}
}
