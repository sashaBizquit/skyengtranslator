//
//  WordMeaningAssembly.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 09.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

enum WordMeaningAssembly: ModuleAssembly {
	typealias View = WordMeaningViewController<Presenter>
	typealias Presenter = WordMeaningPresenter<Router>
	typealias Router = WordMeaningRouter

	static var view: View {
		WordMeaningViewController(presenter: presenter)
	}

	static var presenter: Presenter {
		WordMeaningPresenter(router: router, apiService: ApiServiceManagerAssembly.manager)
	}

	static var router: Router {
		WordMeaningRouter()
	}
}
