//
//  AppDelegate.swift
//  SkyEngTranslator
//
//  Created by Александр Лыков on 07.09.2020.
//  Copyright © 2020 Aleksandr Lykov. All rights reserved.
//

import UIKit

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {
	var window: UIWindow?

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		setupRootViewController(for: application)
		return true
	}

	// MARK: Private

	private func setupRootViewController(for application: UIApplication) {
		let rootWindow = UIWindow(frame: UIScreen.main.bounds)
		rootWindow.rootViewController = UINavigationController(rootViewController: SearchAssembly.view)
		rootWindow.makeKeyAndVisible()
		window = rootWindow
	}
}

